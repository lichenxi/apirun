<?php

    // Récupérer le nom de l'utilisateur actuel
    $username = exec('whoami');

    // Récupérer la distribution utilisée
    $distribution = exec('lsb_release -ds');

    // Récupérer la version du noyau Linux utilisée
    $kernelVersion = exec('uname -r');

    // Récupérer la taille de la mémoire RAM disponible et totale
    $memoryTotal = round(shell_exec('free -m | grep Mem | awk \'{print $2}\'') / 1024);
    $memoryFree = round(shell_exec('free -m | grep Mem | awk \'{print $4}\'') / 1024);

    // Récupérer la taille de stockage disponible et totale
    $storageTotal = round(disk_total_space('/') / (1024 * 1024 * 1024), 2);
    $storageFree = round(disk_free_space('/') / (1024 * 1024 * 1024), 2);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Informations du serveur</title>
</head>
<body>
    <h1>Informations du serveur</h1>

    <p>Nom d'utilisateur actuel : <?php echo $username; ?></p>
    <p>Distribution utilisée : <?php echo $distribution; ?></p>
    <p>Version du noyau Linux utilisée : <?php echo $kernelVersion; ?></p>
    <p>Taille de la mémoire RAM disponible : <?php echo $memoryFree; ?> Go</p>
    <p>Taille totale de la mémoire RAM : <?php echo $memoryTotal; ?> Go</p>
    <p>Taille de stockage disponible : <?php echo $storageFree; ?> Go</p>
    <p>Taille totale de stockage : <?php echo $storageTotal; ?> Go</p>
</body>
</html>

<?php
    echo "<h1>Dix conseils</h1></br>";
    echo "<ol>";
    $connexion = new PDO('pgsql:host=postgresql3;port=5432;dbname=prism', 'snowden', 'nsa');
    $sql = 'SELECT * FROM conseil';
    $results = $connexion->prepare($sql);
    $results->execute();
    while ($row = $results->fetch(PDO::FETCH_ASSOC)){
        echo "<li><b>" . $row['con'] . "</b> : ";
        echo $row['def'] . "</li>";
    }
    echo "</ol>";
?>
